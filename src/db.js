export const home = {
  technology: {
    title: 'Technology',
    description: 'I’m having a love affair with JavaScript at the moment. Some of the frameworks I really enjoy using right now are:',
    list: [
      { title: 'Vue', icon: 'vue' },
      { title: 'React', icon: 'react' },
      { title: 'Node', icon: 'node' },
      { title: 'Express', icon: 'express' }
    ]
  },
  portfolio: {
    title: 'Portfolio',
    description: 'I truly do love to <strong>"do it all"</strong>. A few examples from my portfolio should help you understand what exactly that means.',
    list: [
      {
        title: 'Engage',
        description: 'Web app built with <b><u>Ruby on Rails</u></b>. Project scope was to build, collect, and analyze Employee Opinion Surveys.',
        route: 'engage'
      },
      {
        title: 'Locations',
        description: 'Web app built with <b><u>Vue & Express</u></b>. Project scope was to allow internal users to easily enroll mobile devices into two-factor authentication.',
        route: 'locations'
      },
      {
        title: 'Duo',
        description: 'Mobile app built with <b><u>React Native</u></b>. Project scope was to build a platform for medical providers to communicate, interact with, and follow-up on their customers.',
        route: 'duo'
      },
      {
        title: 'Influur',
        description: 'Coming Soon'
      }
    ]
  },
  skills: {
    title: 'Skills',
    description: 'My life\'s motto is <strong>never stop learning!</strong> Aside from being a passionate Full Stack Developer I\'m skilled in the following areas:',
    list: [
      { title: 'Amazon Web Services', icon: 'aws' },
      { title: 'DevOps / Infrastructure as Code', icon: 'server' },
      { title: 'UX / UI Design', icon: 'ux' },
      { title: 'Product Management', icon: 'product' },
      { title: 'Documentation', icon: 'documentation' },
      { title: 'People Leadership', icon: 'team' }
    ]
  },
  contact: {
    title: 'Contact',
    description: 'You can reach me via social media channels, or by email. Feel free to say hi, hit me up about a potential job, or get my gamer tag.'
  }
}
