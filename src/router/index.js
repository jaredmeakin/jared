import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import ProjectDuo from '@/pages/ProjectDuo'
import ProjectEngage from '@/pages/ProjectEngage'
import ProjectLocations from '@/pages/ProjectLocations'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/duo', name: 'ProjectDuo', component: ProjectDuo },
    { path: '/engage', name: 'ProjectEngage', component: ProjectEngage },
    { path: '/locations', name: 'ProjectLocations', component: ProjectLocations }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
